<h1>PHP Arrays Functions</h1>
<?php
	/*This function accepts a paramter which needs to Output following
		Total Items in array : 10
		Maximum element in array : 100
		Minimum element in array: 1
		Sorted Array ( Ascending Order )
		Sorted Array ( Descending Order )
		Sum Of Array : 
		String : 1 * 23 * 43 * 54 * 51 * 65 * 73 * 82 * 91 * 100

	*/
	function performArrayFunctions($Arr) {
		
		echo"Total Items in array : ".count($Arr);
		echo "</br>Maximum element in array : ". max($Arr);
		echo"</br>Minimum element in array: ". min($Arr); 
		sort($Arr);
		echo"</br>Sorted Array ( Ascending Order )<br>";
		for($i=0; $i< count($Arr); $i++){
			echo $Arr[$i];
			echo"</br>";
		}
		rsort($Arr);
		echo"<br>Sorted Array ( Descending Order )<br>";
		for($i=0; $i< count($Arr); $i++){
			echo $Arr[$i];
			echo"</br>";
		}
			$sum_of_array = array_sum($Arr);
			echo"Sum of Array :". $sum_of_array;
			echo"</br>String :";
			$array_string = implode('*',$Arr);
			echo $array_string;

	}
	$arr = [1,23,43,54,51,65,73,82,91,100]; // global scope
	
	performArrayFunctions($arr); // outputs as described in the doc
?>
<hr>
<h1>PHP String Functions</h1>
<?php
	/*This function accepts a paramter which needs to Output following
		Extracted String = 
		String Length =
		Upper Case String =
		Lower Case String = 
		First Letter Uppercase of Sentence =
		First Letter Uppercase of each word = 
	*/
	
	function performStringFunctions ($str) {
		echo "Extracted String =". substr($str,20);
		echo "</br>String Length =".strlen($str);
		echo "<br>Upper Case String =". strtoupper($str);
		echo "<br>Lower Case String = ". strtolower($str);
		echo "</br>First Letter Uppercase of Sentence =".ucfirst($str);
		echo "</br>First Letter Uppercase of each word =". ucwords($str);
		
	}
	$str = "The quick brown fox jumped over the lazy dog";
	performStringFunctions($str);
	
?>
<hr>
<hr>