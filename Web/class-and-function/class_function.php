<?php
class SimpleClass
{
    function add_two_vars($num1, $num2) {
    	if (!is_numeric($num2)) {
         return "second variable is not integer value";
    	}
    	else if (!is_numeric($num1)) {
         return "first variable is not integer value";
    	}
    	else {
    		return $num1 + $num2;  
    	}
     }
}
$obn = new SimpleClass();
if($_POST){
	if(isset($_POST['num1']) && !empty($_POST['num1']))
	{
		$num1 = $_POST['num1'];
	}
	else
	{
		$num1_error = "Enter the first variable";
	}
	if(isset($_POST['num2']) && !empty($_POST['num2']))
	{
		$num2 = $_POST['num2'];
	}
	else
	{
		$num1_error = "Enter the second variable";
	}
echo $obn->add_two_vars($num1, $num2);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Sum</title>
</head>
<body>
	<form method="post">
			<input type="number" name="num1" placeholder="Enter first integer">
			<input type="number" name="num2" placeholder="Enter second veriable">
			<button>Submit</button>
		</form>
</body>
</html>