
<?php
include 'db_conn.php';
if(isset($_GET['id'])){
  $id = $_GET['id'];
  $querry = "DELETE FROM student WHERE id = '".$id."'";
  mysqli_query($conn, $querry);

}
$querry = "SELECT *FROM Student";
$record = mysqli_query($conn, $querry); 
?>
<!DOCTYPE html>
<html>
<head>
  <title>Dashboard</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
  <div class="container mt-5">
    <h1>Students Records</h1>
    <a href="index.php" class="btn btn-info"> Add Student</a>
    <table class="table table-hover">
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">Email</th>
          <th scope="col">Phone</th>
          <th scope="col">Gender</th>
          <th scope="col">Hobby</th>
          <th scope="col">City</th>      
          <th align="container">Action</th>
        </tr>
      </thead>
      </tbody>
      <?php while ($row = mysqli_fetch_array($record, MYSQLI_ASSOC)) {?>
        <tr>

          <td><?=$row['first_name']. ' '.$row['last_name']?> </td>
          <td><?=$row['email']?></td>
          <td><?=$row['phone_number']?></td>
          <td><?=$row['gender']?></td>
          <td><?=$row['hobby']?></td>
          <td><?=$row['city']?></td>
          <td><button class="btn btn-danger"><a href="?id=<?=$row['id']?>">Delete</a></button></td>
          <td><a href ="update.php?id=<?=$row['id']?>">Edit</a></td>
        </tr>
      <?php }?>
    </table>
  </div>
</body>
</html> 