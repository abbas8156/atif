<?php
include 'db_conn.php';
/*if(isset($_GET['id'])){
  $id = $_GET['id'];
  
}*/

if($_POST){
  $post = $_POST;
  if(isset($post['first_name']) && !empty($post['first_name'])){
    $first_name = $post['first_name'];
  }
  else{
    $first_name_error = "First name is required";
  }
  if(isset($post['last_name']) && !empty($post['last_name'])){
    $last_name = $post['last_name'];
  }
  else{
    $last_name_error = "Last name is required";
  }
  if(isset($post['email']) && !empty($post['email'])){
    $tem_email = $post['email'];
    $duplicat_email = "SELECT * FROM student WHERE email = '".$post['email']."' AND id != '".$_GET['id']."'";
    $chek_email = mysqli_query($conn,$duplicat_email);
      if (mysqli_num_rows($chek_email) > 0) {
         $email_error_exits= 'Email addres is already exist';
          
      }
    else {
      $email = $post['email'];
    }
  }
  else{
    $email_error = "Email is required";
  }
  if(isset($post['password']) && !empty($post['password'])){
    $password = $post['password'];
  }
  else{
    $password_error = "Password is required";
  }
   if(isset($post['phone_number']) && !empty($post['phone_number'])){
    $phone_number = $post['phone_number'];
  }
  else{
    $phone_number_error = "Phone Number is required";
  }

  if(isset($post['city']) && !empty($post['city'])){
        $city =  $post['city'];
    }
    
if(isset($post['hobby']) && !empty($post['hobby'])){
        $hobby =  json_encode($post['hobby']);
    }

if(isset($post['gender']) && !empty($post['gender'])){
        $gender =  $post['gender'];
    }

  if(isset($first_name) && isset($last_name) && isset($email) && isset($password) && isset($phone_number)) {


$data_insert = "UPDATE student SET first_name = '".$first_name."', last_name = '".$last_name."', email = '".$email."', password = '".$password."', phone_number = '".$phone_number."', hobby = '".$hobby."', city = '".$city."', gender = '".$gender."' WHERE id = '".$_GET['id']."'";
      if ($conn->query($data_insert) === TRUE) {
         header('location:dashboard.php');
        } 
        else {
        echo "Error: " . $data_insert . "<br>" . $conn->error;
  }
}
}


$querry = "SELECT * FROM student WHERE id = '".$_GET['id']."'";
$record = mysqli_query($conn, $querry); 
$row = mysqli_fetch_array($record, MYSQLI_ASSOC);
$hobbies = json_decode($row['hobby']);
?>


<!DOCTYPE html>
<html>
<head>
  <title>First Page</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


  <style type="text/css">
    .red
    {
      color: red;
    }
    .border-red
    {
      border-color: red !important;
    }
  </style>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-3"></div>
<div class ="col-md-6 border mt-5">
<form method ="POST" action ="" >
  <div class="form-group <?=isset($first_name_error)?'red':''?>"
    <label for="first_name">First Name</label>
    <input type="text" class="form-control <?=isset($first_name_error)?'border-red':''?>" id="first_name" placeholder="Enter First Name" name="first_name" value="<?=$row['first_name']?>" >
    <?=isset($first_name_error)?$first_name_error:''?>
  </div>
  <div class="form-group <?=isset($last_name_error)?'red':''?>">
    <label for="last_name">Last Name</label>
    <input type="text" class="form-control  <?=isset($last_name_error)?'border-red':''?>" id="last_name" placeholder="Enter last Name" name="last_name"  value="<?=$row['last_name']?>"  >
        <?=isset($last_name_error)?$last_name_error:''?>

  </div>
  <div class="form-group <?=(isset($email_error) || isset($email_error_exits))?'red':''?>">
    <label for="last_name">Email Address</label>
    <input type="email" class="form-control  <?=(isset($email_error) || isset($email_error_exits))?'border-red':''?>" id="email" placeholder="Enter your email" name="email"  value="<?=$row['email']?>">
        <?=isset($email_error)?$email_error:''?>
        <?=isset($email_error_exits)?$email_error_exits:''?>

  </div>
  <div class="form-group ">
     <label for="city">City</label>
      <select id="city" class="form-control" name="city">
        <option <?=$row['city']=='Karachi'?'selected':''?> >Karachi</option>
        <option <?=$row['city']=="Lahore"?'selected':''?> >Lahore</option>
        <option  <?=$row['city']=="Islamabad"?'selected':''?>>Islamabad</option>
      </select>
      </div>
  <div class="form-group ">
     <label for="checkbox">Select Hobby</label>

  <input class="form-group" type="checkbox" id="inlineCheckbox1" <?=in_array('Cricket',$hobbies)?'checked':''?> name="hobby[]" value="Cricket">
  <label class="form-group" for="inlineCheckbox1">Cricket</label>
  <input class="form-group" type="checkbox" id="inlineCheckbox2" <?=in_array('PUBG',$hobbies)?'checked':''?> name="hobby[]" value="PUBG">
  <label class="form-group" for="inlineCheckbox2">PUBG</label>
 <input class="form-group" type="checkbox" id="inlineCheckbox3" <?=in_array('Programming',$hobbies)?'checked':''?> name="hobby[]" value="Programming" >
  <label class="form-group" for="inlineCheckbox3">Programming</label>
 </div>
  <div class="form-group">
    <label for="last_name">Gender</label>
     <input class="form-group" type="radio" name="gender" id="inlineRadio1" value="male" <?=$row['gender']=='male'?'checked':''?> >
  <label class="form-group" for="inlineRadio1">Male</label>
   <input class="form-group" type="radio" name="gender" id="inlineRadio2" value="female" <?=$row['gender']=='female'?'checked':''?>>
  <label class="form-group" for="inlineRadio2">Female</label>
  </div>
  <div class="form-group <?=isset($password_error)?'red':''?>">
    <label for="last_name">Password</label>
    <input type="password" class="form-control  <?=isset($password_error)?'border-red':''?>" id="password" placeholder="Enter Your Password" name="password"  value="<?=$row['password']?>" >
        <?=isset($password_error)?$password_error:''?>

  </div>
  <div class="form-group <?=isset($phone_number_error)?'red':''?>">
    <label for="last_name">Phone Number</label>
    <input type="number" class="form-control  <?=isset($phone_number_error)?'border-red':''?>" id="phone_number" placeholder="Enter Phone number" name="phone_number"  value="<?=$row['phone_number']?>" >
        <?=isset($phone_number_error)?$phone_number_error:''?>

  </div>
  <div class="form-group d-flex justify-content-center ">
  <button type="submit" class="btn btn-primary w-50" name="submit">Submit</button>
  </div>
</form>
</div>
</div>
</div>

</body>
</html> 

