<?php 
include 'db_conn.php';

if($_POST){
    $post = $_POST;

if(isset($post['first_name']) && !empty($post['first_name'])){
        $first_name =  $post['first_name'];
    }
else{
    
    $first_name_error = "first Name is Required";
}
if(isset($post['last_name']) && !empty($post['last_name'])){
        $last_name =  $post['last_name'];
    }
else{
    
    $last_name_error = "Last Name is Required";
}
if(isset($post['email']) && !empty($post['email'])){
    $tem_email = $post['email'];
    $duplicat_email = "SELECT * FROM student WHERE email = '".$post['email']."'";
    $chek_email = mysqli_query($conn,$duplicat_email);
      if (mysqli_num_rows($chek_email) > 0) {
         $email_error_exist = 'Email addres is already exist';
          
      }
    else {
      $email = $post['email'];
    }
  }
  else{
    $email_error = "Email is required";
  }
if(isset($post['password']) && !empty($post['password'])){
        $password =  $post['password'];
    }
else{
    
    $password_error = "Password is Required";
}
if(isset($post['phone_number']) && !empty($post['phone_number'])){
        $phone_number =  $post['phone_number'];
    }
else{
    
    $phone_number_error = "Phone Numer is not required";
}
if(isset($post['city']) && !empty($post['city'])){
        $city =  $post['city'];
    }
    
if(isset($post['hobby']) && !empty($post['hobby'])){
        $hobby =  json_encode($post['hobby']);
    }

if(isset($post['gender']) && !empty($post['gender'])){
        $gender =  $post['gender'];
    }

if(isset($first_name) && isset($last_name) && isset($email) && isset($password) && isset($phone_number)) {

$data_insert = "INSERT INTO student (first_name, last_name, email, password, phone_number, city, hobby, gender) VALUES ('".$first_name."','".$last_name."','".$email."','".$password."','".$phone_number."', '".$city."', '".$hobby."', '".$gender."')";
      if ($conn->query($data_insert) === TRUE) {
         header('location:dashboard.php');
        } 
        else {
        echo "Error: " . $data_insert . "<br>" . $conn->error;
  }
}
}

?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<style type="text/css">
    .red{
        color:red;
    }
    .border-red{
        border-color: red;
    }
</style>
<title>First Page</title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-3"></div>
<div class ="col-md-6 border mt-5">
        <h3 align="center">Create Account</h3>

<form method ="POST" action ="" >
 <div class="form-group <?=isset($first_name_error)?'red':''?>">
    <label for="first_name">First Name</label>
    <input type="text" class="form-control <?=isset($first_name_error)?'border-red':''?>" id="first_name" placeholder="Enter First Name" name="first_name" value="<?=isset($first_name)?$first_name:''?>" >
    <?=isset($first_name_error)?$first_name_error:''?>
  </div>
  <div class="form-group <?=isset($last_name_error)?'red':''?>">
    <label for="last_name">Last Name</label>
    <input type="text" class="form-control <?=isset($last_name_error)?'border-red':''?>" id="last_name" placeholder="Enter last Name" name="last_name" value = "<?=isset($last_name)?$last_name:''?>" >
    <?=isset($last_name_error)?$last_name_error:''?>
  </div>
  <div class="form-group <?=(isset($email_error) || isset($email_error_exist))?'red':''?>">
    <label for="last_name">Email Address</label>
    <input type="email" class="form-control  <?=(isset($email_error) || isset($email_error_exist))?'border-red':''?>" id="email" placeholder="Enter your email" name="email"  value="<?=isset($email)?$email:''?><?=isset($tem_email)?$tem_email:''?>">
        <?=isset($email_error)?$email_error:''?>
        <?=isset($email_error_exist)?$email_error_exist:''?>

  </div>
  
  <div class="form-group ">
     <label for="city">City</label>
      <select id="city" class="form-control" name="city" value = "<?=isset($city)?$city:''?>">
        <option >Karachi</option>
        <option selected>Lahore</option>
        <option>Islamabad</option>
      </select>
      </div>

<div class="form-group ">
     <label for="checkbox">Select Hobby</label>

  <input class="form-group" type="checkbox" id="inlineCheckbox1" name="hobby[]" value="Cricket">
  <label class="form-group" for="inlineCheckbox1">Cricket</label>
  <input class="form-group" type="checkbox" id="inlineCheckbox2" name="hobby[]" value="PUBG">
  <label class="form-group" for="inlineCheckbox2">PUBG</label>
 <input class="form-group" type="checkbox" id="inlineCheckbox3" name="hobby[]" value="Programming" >
  <label class="form-group" for="inlineCheckbox3">Programming</label>
 </div>
  <div class="form-group">
    <label for="last_name">Gender</label>
     <input class="form-group" type="radio" name="gender" id="inlineRadio1" value="male" checked>
  <label class="form-group" for="inlineRadio1">Male</label>
   <input class="form-group" type="radio" name="gender" id="inlineRadio2" value="female">
  <label class="form-group" for="inlineRadio2">Female</label>
  </div>
  <div class="form-group <?=isset($password_error)?'red':''?>">
    <label for="password">Password</label>
    <input type="password" class="form-control <?=isset($password_error)?'border-red':''?>" id="password" placeholder="Enter Your Password" name="password" value="<?=isset($password)?$password:''?>" >
    <?=isset($password_error)?$password_error:''?>
  </div>
  <div class="form-group <?=isset($phone_number_error)?'red':''?>">
    <label for="last_name">Phone Number</label>
    <input type="number" class="form-control <?=isset($phone_number_error)?'border-red':''?>" id="phone_number" placeholder="Enter Phone number" name="phone_number" value="<?=isset($phone_number)?$phone_number:''?>">
    <?=isset($phone_number_error)?$phone_number_error:''?>
  </div>
  <div class="form-group d-flex justify-content-center ">
  <button type="submit" class="btn btn-primary w-50" name="submit">Submit</button>
  </div>
</form>
</div>
</div>
</div>

</body>
</html> 

