<?php
/* Q.1 Write program to show desired output as following */

/* Use all conditional operators between $a and $b to display following Output. 
TEST1 : Either a or b is false 
TEST2 : Either a or b is false 
TEST3 : Either a or b is true 
TEST4 : Either a or b is true 
TEST5 : a is true  
TEST6 : b is true  
TEST7 : a is false 
TEST8 : b is false */
function conditionalOperators() {
	$a = 42;  $b = 0;

	//TEST1 : Either a or b is false 
	if( $a && $b ) {            
		echo "TEST1 : Both a and b are true<br/>";
	}
	else {             
		echo "TEST1 : Either a or b is false<br/>";
	} 
	// TEST2 : Either a or b is false 
	$b= true;
	$a = false;
	if( $a && $b ) {            
		echo "TEST2 : Both a and b are true<br/>";
	}
	else {             
		echo "TEST2 : Either a or b is false<br/>";
	
	} 
	$b= 5;
	$a = 0;
	if( $a  && $b ) {            
		echo "TEST3 : Both a and b are true<br/>";
	}
	else {             
		echo "TEST3 : Either a or b is True<br/>";
	
	} 
	$b= 5;
	$a = 0;
	if( $a && $b ) {            
		echo "TEST4 : Both a and b are true<br/>";
	}
	else {             
		echo "TEST4 : Either a or b is True<br/>";
	
	} 
	$b= false;
	$a = true;
	if( $a && $b ) {            
		echo "TEST5 : Both a and b are true<br/>";
	}
	else {             
		echo "TEST5 : a is true<br/>";
	
	} 
	$b= true;
	$a = false;
	if( $a && $b ) {            
		echo "TEST5 : Both a and b are true<br/>";
	}
	else {             
		echo "TEST5 : b is true<br/>";
	
	} 

	$b= true;
	$a = false;
	if( $a && $b ) {            
		echo "TEST5 : Both a and b are true<br/>";
	}
	else {             
		echo "TEST5 : a is false<br/>";
	
	} 

	$b= true;
	$a = false;
	if( $a && $b ) {            
		echo "TEST5 : Both a and b are true<br/>";
	}
	else {             
		echo "TEST5 : b is false<br/>";
	
	} 

	
}


function numberInSingleLine () {
// your code is here
  $x = 0;$y = 1;$z = 2;$a = 3;
  echo $x.$y.$z.$a;
}

function iterateVariables () {

   for ($x = 1; $x <= 10; $x++) {
    
    if($x==4 || $x==6) {
    	continue;
    }
	else {
		echo "The number is: $x <br>";
	}
	if($x==8){
	break;
	}
   }
}

echo "<h1>Conditional If-Else Task:</h1>";
conditionalOperators();

echo "<h1>Print Numbers Task:</h1>";
numberInSingleLine();

echo "<h1>Loop base Task:</h1>";
iterateVariables();
?>