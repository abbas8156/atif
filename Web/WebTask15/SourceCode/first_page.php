<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<title>First Page</title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-3"></div>
<div class ="col-md-6 border mt-5">
<form method ="POST" action ="" >
  <div class="form-group">
    <label for="first_name">First Name</label>
    <input type="text" class="form-control" id="first_name" placeholder="Enter First Name" name="first_name" required>
  </div>
  <div class="form-group">
    <label for="last_name">Last Name</label>
    <input type="text" class="form-control" id="last_name" placeholder="Enter last Name" name="last_name" required>
  </div>
  <div class="form-group">
    <label for="last_name">Email Address</label>
    <input type="email" class="form-control" id="email" placeholder="Enter your email" name="email" required>
  </div>
  <div class="form-group">
    <label for="last_name">Password</label>
    <input type="password" class="form-control" id="password" placeholder="Enter Your Password" name="password" required>
  </div>
  <div class="form-group">
    <label for="last_name">Phone Number</label>
    <input type="number" class="form-control" id="phone_number" placeholder="Enter Phone number" name="phone_number" required>
  </div>
  <div class="form-group d-flex justify-content-center ">
  <button type="submit" class="btn btn-primary w-50" name="submit">Submit</button>
  </div>
</form>
</div>
</div>
</div>

</body>
</html> 

