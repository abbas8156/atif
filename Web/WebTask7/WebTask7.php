<h1>Local and Global Scope</h1>
<?php
	$number = 10; // global scope
	

	/*This function accepts a paramter which needs to multiplied with a global variable*/
	function multiply ($multiplyBy) {
		$putw = $multiplyBy*5;
	return $putw;
	}
	echo "Number Output: " . multiply ($number);
?>
<hr>
<h1>Static Variables</h1>
<?php
	/* This function has a number variable. Use Static variable power so that
	variable value stays in memory */

	function printNumber () {
	static $number = 0; // declare static variable
	$number = $number +5;
	echo $number."</br>";
	}
	
	printNumber(); // 0
	printNumber(); // 5
	printNumber(); // 10
	printNumber();
	printNumber();
	printNumber();
	printNumber();


?>
<hr>
<hr>