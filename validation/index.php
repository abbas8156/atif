<?php
include 'database_con.php';
if($_POST){
  $value = $_POST;
  if(isset($value['first_name']) && !empty($value['first_name'])){
    $first_name = $value['first_name'];
  }
  else{
    $first_name_error = "First name is required";
  }
  if(isset($value['last_name']) && !empty($value['last_name'])){
    $last_name = $value['last_name'];
  }
  else{
    $last_name_error = "Last name is required";
  }
  if(isset($value['email']) && !empty($value['email'])){
    $tem_email = $value['email'];
    $duplicat_email = "SELECT * FROM student WHERE email = '".$value['email']."'";
    $chek_email = mysqli_query($conn,$duplicat_email);
      if (mysqli_num_rows($chek_email) > 0) {
         $email_error_exist = 'Email addres is already exist';
          
      }
    else {
      $email = $value['email'];
    }
  }
  else{
    $email_error = "Email is required";
  }
  if(isset($value['password']) && !empty($value['password'])){
    $password = $value['password'];
  }
  else{
    $password_error = "Password is required";
  }
   if(isset($value['phone_number']) && !empty($value['phone_number'])){
    $phone_number = $value['phone_number'];
  }
  else{
    $phone_number_error = "Phone Number is required";
  }
  if(isset($first_name) && isset($last_name) && isset($email) && isset($password) && isset($phone_number)) {

$data_insert = "INSERT INTO student (first_name, last_name, email, password, phone_number) VALUES ('".$first_name."','".$last_name."','".$email."','".$password."','".$phone_number."')";
      if ($conn->query($data_insert) === TRUE) {
         header('location:dashboard.php');
        } 
        else {
        echo "Error: " . $data_insert . "<br>" . $conn->error;
  }
}
}
?>


<!DOCTYPE html>
<html>
<head>
  <title>First Page</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


  <style type="text/css">
    .red
    {
      color: red;
    }
    .border-red
    {
      border-color: red !important;
    }
  </style>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-3"></div>
<div class ="col-md-6 border mt-5">
<form method ="POST" action ="" >
  <div class="form-group <?=isset($first_name_error)?'red':''?>"
    <label for="first_name">First Name</label>
    <input type="text" class="form-control <?=isset($first_name_error)?'border-red':''?>" id="first_name" placeholder="Enter First Name" name="first_name" value="<?=isset($first_name)?$first_name:''?>" >
    <?=isset($first_name_error)?$first_name_error:''?>
  </div>
  <div class="form-group <?=isset($last_name_error)?'red':''?>">
    <label for="last_name">Last Name</label>
    <input type="text" class="form-control  <?=isset($last_name_error)?'border-red':''?>" id="last_name" placeholder="Enter last Name" name="last_name"  value="<?=isset($last_name)?$last_name:''?>"  >
        <?=isset($last_name_error)?$last_name_error:''?>

  </div>
  <div class="form-group <?=(isset($email_error) || isset($email_error_exist))?'red':''?>">
    <label for="last_name">Email Address</label>
    <input type="email" class="form-control  <?=(isset($email_error) || isset($email_error_exist))?'border-red':''?>" id="email" placeholder="Enter your email" name="email"  value="<?=isset($email)?$email:''?><?=isset($tem_email)?$tem_email:''?>">
        <?=isset($email_error)?$email_error:''?>
        <?=isset($email_error_exist)?$email_error_exist:''?>

  </div>
  <div class="form-group <?=isset($password_error)?'red':''?>">
    <label for="last_name">Password</label>
    <input type="password" class="form-control  <?=isset($password_error)?'border-red':''?>" id="password" placeholder="Enter Your Password" name="password"  value="<?=isset($password)?$password:''?>" >
        <?=isset($password_error)?$password_error:''?>

  </div>
  <div class="form-group <?=isset($phone_number_error)?'red':''?>">
    <label for="last_name">Phone Number</label>
    <input type="number" class="form-control  <?=isset($phone_number_error)?'border-red':''?>" id="phone_number" placeholder="Enter Phone number" name="phone_number"  value="<?=isset($phone_number)?$phone_number:''?>" >
        <?=isset($phone_number_error)?$phone_number_error:''?>

  </div>
  <div class="form-group d-flex justify-content-center ">
  <button type="submit" class="btn btn-primary w-50" name="submit">Submit</button>
  </div>
</form>
</div>
</div>
</div>

</body>
</html> 

