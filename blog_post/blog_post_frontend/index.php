<?php
include 'header.php';
include '../template/db_conn.php';
include '../template/controller/blog_controller.php';
$blog_list = new Blog_controller($conn);
$blog_record = $blog_list->listBlogWithUser();
?>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Clean Blog</h1>
            <span class="subheading">A Blog Theme by Start Bootstrap</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="post-preview">
          <?php while ($row = mysqli_fetch_array($blog_record, MYSQLI_ASSOC)) {?>
          <a href="post.html">
            <h2 class="post-title">
             <?=$row['blog_title']?>
             </h2>
             <hr>
              <img src="../template/assets/img/<?=$row['image']?>" width="300" height= "300">
           
            <p class="post-subtitle">
            <?=$row['blog_description']?>
          </p>
          </a>
          <p class="post-meta">Posted by
            <a href="#"><?=$row['user_name']?></a>
            on <?=$row['created_at']?></p>
          <?php }?>

        <hr>
        
        <hr>
        <!-- Pager -->
        <div class="clearfix">
          <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>

  <hr>

  <!-- Footer -->
  <?php include 'footer.php'?>