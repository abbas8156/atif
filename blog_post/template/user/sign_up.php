<?php
include '..//db_conn.php';
include '..//controller/user_controller.php';
$user_signup = new User_Controller($conn);
if($_POST) {
  $value = $_POST;
  if(isset($value['user_name']) && !empty($value['user_name'])) {
    $user_name = $value['user_name'];
  }
  else {
    $user_name_error = "User name is required";
  }
  if(isset($value['user_email']) && !empty($value['user_email'])) {
    $tem_email = $value['user_email'];
    $duplicat_email = "SELECT * FROM tbl_user WHERE user_email = '".$value['user_email']."'";
    $chek_email = mysqli_query($conn,$duplicat_email);
      if (mysqli_num_rows($chek_email) > 0) {
         $email_error_exist = 'Email addres is already exist';
       }
       else {
        $user_email = $value['user_email'];
      }
    }
    else {
      $email_error = "Email is required";
    }
    if(isset($value['password']) && !empty($value['password'])) {
      $password = $value['password'];
    }
    else {
      $password_error = "Password is required";
    }
    if(isset($value['user_ph_num']) && !empty($value['user_ph_num'])) {
      $user_ph_num = $value['user_ph_num'];
    }
    else {

      $user_ph_num_error = "Phone Number is required";
    }
    if (isset($_FILES['user_image'])) {
        $name = $_FILES['user_image']['name'];
        $target_dir = "../assets/img/profile_img/";
        $target_file = $target_dir . basename($_FILES["user_image"]["name"]);
          if(move_uploaded_file($_FILES['user_image']['tmp_name'],$target_dir.$name)) {
          $user_image = true;
        }
          else {

            $image_error = "Please select an image";
          }
        }
          if(isset($user_name) && isset($user_email) && isset($password) && isset($user_ph_num) && $user_image) {
            if($user_signup->registerUser($user_name, $user_email, $password, $user_ph_num, $name)) {
              echo '<script>alert("You have registed successuflly Please sign in to acess your profile");
              location.href = "user_login.php";
              </script>';
            } 
            else {
              echo "Error: ".$conn->error;
            }
          }
        }
      
?>


<!DOCTYPE html>
<html>
<head>
  <title>First Page</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


  <style type="text/css">
    .red
    {
      color: red;
    }
    .border-red
    {
      border-color: red !important;
    }
  </style>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-3"></div>
        <div class ="col-md-6 border mt-5">
          <form method ="POST" enctype="multipart/form-data">
              <div class="form-group <?=isset($user_name_error)?'red':''?>"
                <label for="user_name">User Name</label>
                  <input type="text" class="form-control <?=isset($user_name_error)?'border-red':''?>" id="first_name" placeholder="Enter User Name" name="user_name" value="<?=isset($user_name)?$user_name:''?>" >
                  <?=isset($user_name_error)?$user_name_error:''?>
              </div>
              <div class="form-group <?=(isset($email_error) || isset($email_error_exist))?'red':''?>">
                <label for="user_email">User Email</label>
                  <input type="email" class="form-control  <?=(isset($email_error) || isset($email_error_exist))?'border-red':''?>" id="email" placeholder="Enter your email" name="user_email"  value="<?=isset($email)?$email:''?><?=isset($tem_email)?$tem_email:''?>">
                  <?=isset($email_error)?$email_error:''?>
                  <?=isset($email_error_exist)?$email_error_exist:''?>
              </div>
              <div class="form-group <?=isset($password_error)?'red':''?>">
                <label for="last_name">Password</label>
                  <input type="password" class="form-control  <?=isset($password_error)?'border-red':''?>" id="password" placeholder="Enter Your Password" name="password"  value="<?=isset($password)?$password:''?>" >
                  <?=isset($password_error)?$password_error:''?>

              </div>
              <div class="form-group <?=isset($user_ph_num_error)?'red':''?>">
                <label for="last_name">Phone Number</label>
                  <input type="number" class="form-control  <?=isset($user_ph_num_error)?'border-red':''?>" id="user_ph_num" placeholder="Enter Phone number" name="user_ph_num"  value="<?=isset($user_ph_num)?$user_ph_num:''?>" >
                  <?=isset($user_ph_num_error)?$user_ph_num_error:''?>
                </div>
                <div class="form-group <?=isset($image_error)?'red':''?>">
                  <label for="user_image">Profile Image</label>
                  <input type="file" name="user_image" accept="image/*" class="form-control <?=isset($image_error)?'border-red':''?>">
                  <?=isset($image_error)?$image_error:''?>
                </div>
              <div class="form-group d-flex justify-content-center ">
                <button type="submit" class="btn btn-primary w-50" name="submit">Submit</button>
              </div>
          </form>
        </div>
      </div>
    </div>
  </body>
</html> 
