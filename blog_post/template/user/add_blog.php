<?php
	include '../db_conn.php';
	include 'header.php';
	include '../controller/blog_controller.php';
	$new_blog = new Blog_controller($conn);
	if($_POST) {
		$value = $_POST;
		if(isset($value['blog_title']) && !empty($value['blog_title'])) {
		    $blog_title = $value['blog_title'];
		}
		else {
		    $blog_title_error = "Blog tiltle is required";
		}
		if(isset($value['blog_heading']) && !empty($value['blog_heading'])) {
		    $blog_heading = $value['blog_heading'];
		}
		else {
		    $blog_heading_error = "Blog heading is required";
		}
		if(isset($value['blog_description']) && !empty($value['blog_description'])) {
		    $blog_description = $value['blog_description'];
		}
		else {
	    	$blog_description_error = "Blog description is required";
	    }
	    if (isset($_FILES['image'])) {
	    	$name = $_FILES['image']['name'];
	    	$target_dir = "../assets/img/";
	    	$target_file = $target_dir . basename($_FILES["image"]["name"]);
	    	if(move_uploaded_file($_FILES['image']['tmp_name'],$target_dir.$name)) {
	    		$image = true;
	    	}
	    	else
	    	{
	    	$image_error = "Please select an image";
	    	}

	    }
	}
	if(isset($blog_title) && isset($blog_description) &&isset($blog_heading) && $image) {
		if($new_blog->addNewBlog($blog_title, $blog_heading, $blog_description,$name)) {
			echo '<script>alert("New Blog is added successfully Thanks");location.href = "add_blog.php";</script>';
		} 
		else {
		    echo "Error: ".$conn->error;
		}
	}
?>
<body>
	<div id="wrapper">
		<?php include 'top_navigation.php'?>
			<?php include 'sidebar_navigation.php'?>
				<div class="main">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">Add Blog</h3>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-2"></div>
											<div class="col-md-8">
												<form method="POST" enctype="multipart/form-data">
													<div class="form-group <?=isset($blog_title_error)?'red':''?>">
														<label for="text">Title</label>
														<input type="text" class="form-control  <?=isset($blog_title_error)?'border-red':''?>" id="blog_title" name = "blog_title"  maxlength = "50">
														 <?=isset($blog_title_error)?$blog_title_error:''?>
													</div>
													<div class="form-group <?=isset($blog_heading_error)?'red':''?>">
														<label for="text">Heading</label>
														<input type="text" class="form-control  <?=isset($blog_heading_error)?'border-red':''?>" id="blog_title" name = "blog_heading"  maxlength = "100" >
														 <?=isset($blog_heading_error)?$blog_heading_error:''?>
													</div>
													<div class="form-group <?=isset($blog_description_error)?'red':''?>">
														<label for="exampleFormControlTextarea1">Description</label>
														<textarea class="form-control rounded-0 <?=isset($blog_description_error)?'border-red':''?>" id="blog_description" rows="10" name="blog_description"></textarea>
														 <?=isset($blog_description_error)?$blog_description_error:''?>
													</div>
													<div class="form-group <?=isset($image_error)?'red':''?>">
														<label for="exampleFormControlTextarea1">Image</label>
														<input type="file" name="image" accept="image/*" class="form-control <?=isset($image_error)?'border-red':''?>">
														<?=isset($image_error)?$image_error:''?>
													</div>
													<div>
														<button type="submit" class="btn btn-primary">Submit</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</table>
	</div>
<?php include 'footer.php'?>