<?php
include '../db_conn.php';
include '../controller/user_controller.php';
include 'header.php';
$user_update = new User_Controller($conn);
if($_POST) {
  $value = $_POST;
  if(isset($value['user_name']) && !empty($value['user_name'])) {
    $user_name = $value['user_name'];
  }
  else {
    $user_name_error = "User name is required";
  }

  if(isset($value['user_email']) && !empty($value['user_email'])){
    $tem_email = $value['user_email'];
    $duplicat_email = "SELECT * FROM tbl_user WHERE user_email = '".$value['user_email']."' AND user_id != '".$_SESSION['user_id']."'";
    $chek_email = mysqli_query($conn,$duplicat_email);
      if (mysqli_num_rows($chek_email) > 0) {
         $email_error_exist = 'user_email addres is already exist';
          
      }
    else {
      $user_email = $value['user_email'];
    }
  }
  else{
    $email_error = "user_email is required";
  }
   if (isset($_FILES['user_image'])) {
        $name = $_FILES['user_image']['name'];
        $target_dir = "../assets/img/profile_img/";
        $target_file = $target_dir . basename($_FILES["user_image"]["name"]);
          if(move_uploaded_file($_FILES['user_image']['tmp_name'],$target_dir.$name)) {
          $user_image = true;
      }
    }
   if(isset($value['user_ph_num']) && !empty($value['user_ph_num'])) {
    $user_ph_num = $value['user_ph_num'];
  }
  else {
    $user_ph_num_error = "Phone Number is required";
  }
  if(isset($user_name)  && isset($user_email) && isset($user_ph_num)) {
  	if (isset($user_image)) {

  		$res = $user_update->updateUser($user_name, $user_email, $user_ph_num, $name);
  	}
  	else
  	{
  		$res = $user_update->updateUser($user_name, $user_email, $user_ph_num);
  	}
   	
	  	if($res) 
	  	{

	         header('location:profile_page.php');
	        } 
	        else {
	        echo "Error: " . $data_insert . "<br>" . $conn->error;
	  }
}
}
// $querry = "SELECT * FROM tbl_user WHERE user_id = '".$_SESSION['user_id']."'";
// $record = mysqli_query($conn, $querry); 
// $row = mysqli_fetch_array($record, MYSQLI_ASSOC);
?>
<body>
	<div id="wrapper">
		<?php include 'top_navigation.php'?>
			<?php include 'sidebar_navigation.php'?>
				<div class="main">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">Edit User</h3>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-2"></div>
											<div class="col-md-8">
												<form method ="POST" enctype="multipart/form-data">
									              <div class="form-group <?=isset($user_name_error)?'red':''?>"
									                <label for="user_name">User Name</label>
									                  <input type="text" class="form-control <?=isset($user_name_error)?'border-red':''?>" id="user_name" placeholder="Enter User Name" name="user_name" value="<?=$_SESSION['user_name']?>">
									                  <?=isset($user_name_error)?$user_name_error:''?>
									              </div>
									              <div class="form-group <?=(isset($email_error) || isset($email_error_exist))?'red':''?>">
									                <label for="user_email">User user_email</label>
									                  <input type="user_email" class="form-control  <?=(isset($email_error) || isset($email_error_exist))?'border-red':''?>" id="user_email" placeholder="Enter your user_email" name="user_email"  value="<?=$_SESSION['user_email']?>">
									                  <?=isset($email_error)?$email_error:''?>
									                  <?=isset($email_error_exist)?$email_error_exist:''?>
									              </div>
									              <div class="form-group <?=isset($user_ph_num_error)?'red':''?>">
									                <label for="last_name">Phone Number</label>
									                  <input type="number" class="form-control  <?=isset($user_ph_num_error)?'border-red':''?>" id="user_ph_num" placeholder="Enter Phone number" name="user_ph_num"  value="<?=$_SESSION['user_ph_num']?>" >
									                  <?=isset($user_ph_num_error)?$user_ph_num_error:''?>
									                </div>
									                <div class="form-group>">
									                  <label for="user_image">Profile Image</label>
									                  <input type="file" name="user_image" accept="image/*" class="form-control">
									          <img src=../assets/img/profile_img/<?=$_SESSION['user_image']?>>
									                </div>
									              <div class="form-group d-flex justify-content-center ">
									                <button type="submit" class="btn btn-primary w-50" name="submit">Submit</button>
									              </div>
									          </form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</table>
	</div>
<?php include 'footer.php'?>