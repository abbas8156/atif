<?php
include '../db_conn.php';
	include 'header.php';
	include '../controller/blog_controller.php';

	$blog = new Blog_controller($conn);
	if(isset($_GET['id'])){
  $id = $_GET['id'];
	$blog->deleteBlog($id);
}
if(isset($_GET['blog_id']) && isset($_GET['status'])){
  $blog_id = $_GET['blog_id'];
  $status = $_GET['status'];
  $blog->statusBlog($blog_id, $status);
  }
  $blog_record = $blog->listUserBlog();

?>
	<body>
			<!-- WRAPPER -->
			<div id="wrapper">
		<!-- NAVBAR -->
				<?php include 'top_navigation.php'?>
					<!-- SIDE NAVBAR -->
					<?php include 'sidebar_navigation.php'?>
						<div class="main">
						<!-- 	<div class="col-md-6"> -->
							<!-- TABLE HOVER -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Blog List</h3>
								</div>
								<div class="panel-body">
									<table class="table table-hover">
										<thead>
											<tr>
												<th>ID</th>
												<th>Title</th>
												<th>Heading</th>
												<th>Description</th>
												<th>Created At</th>
												<th>Updated At</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php while ($row = mysqli_fetch_array($blog_record, MYSQLI_ASSOC)) {?>
       								 <tr>
       								 	<td><?=$row['blog_id']?></td>
							         	<td><?=$row['blog_title']?></td>
							         	<td><?=$row['blog_heading']?></td>
							          	<td><?=$row['blog_description']?></td>
							          	<td><?=$row['created_at']?></td>
							          	<td><?=$row['updated_at']?></td>
							          	<td><a href="?blog_id=<?=$row['blog_id']?>&status=<?=$row['is_active']?0:1?>"><?=$row['is_active']?'Block':'Active'?></td>
							          	<td><a href="?id=<?=$row['blog_id']?>">Delete</td>
							          	<td><a href="blog_edit_page.php">Edit</td>
							        </tr>
						  	 <?php }?>
											
										</tbody>
									</table>
								</div>
							</div>
							<!-- END TABLE HOVER -->
						</div>
					</div>
						    	
							</table>
					<!-- 	</div> -->
			</div>
	
<?php include 'footer.php'?>