<?php
include '../db_conn.php';
include '../controller/user_controller.php';
include 'header.php';
// $querry = "SELECT * FROM tbl_user WHERE user_id = '".$_SESSION['user_id']."'";
// $record = mysqli_query($conn, $querry); 
// $row = mysqli_fetch_array($record, MYSQLI_ASSOC);
?>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!--TOP NAVBAR -->
		<?php include'top_navigation.php'?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?PHP include 'sidebar_navigation.php'?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-profile">
								<div class="clearfix">
									<h3 class="panel-title">User Profile</h3>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-2"></div>
											<div class="col-md-8">
									<div class="profile-header">
										<div class="overlay"></div>
										<div class="profile-main">
											<img src="../assets/img/profile_img/<?=$_SESSION['user_image']?>" class="img-circle" alt="Avatar">
											<h3 class="name"><?=$_SESSION['user_name']?></h3>
										</div>
									</div>
									<!-- END PROFILE HEADER -->
									<!-- PROFILE DETAIL -->
									<div class="profile-detail">
										<div class="profile-info">
											<h4 class="heading">Basic Info</h4>
											<ul class="list-unstyled list-justify">
												<li>Mobile Number <span><?=$_SESSION['user_ph_num']?></span></li>
												<li>Email Address<span><?=$_SESSION['user_email']?></span></li>
										</div>
										<div class="profile-info">
										</div>
											<h4 class="heading">Social</h4>
											<ul class="list-inline social-icons">
												<li><a href="https://www.facebook.com/login/" class="facebook-bg" target="blank"><i class="fa fa-facebook"></i></a></li>
												<li><a href="https://twitter.com/login?lang=en" class="twitter-bg"  target="blank"><i class="fa fa-twitter"></i></a></li>
												<li><a href="https://accounts.google.com/" class="google-plus-bg"  target="blank"><i class="fa fa-google-plus"></i></a></li>
												<li><a href="https://github.com/login" class="github-bg"  target="blank"><i class="fa fa-github"></i></a></li>
											</ul>
												
										</div>
										<div class="profile-info">
											<h4 class="heading">About</h4>
											<p>Interactively fashion excellent information after distinctive outsourcing.</p>
										</div>
										<div class="text-center"><a href="profile_edit_page.php" class="btn btn-primary">Edit Profile</a></div>
									</div>
								<!-- END PROFILE DETAIL -->
								</div>	
							</div>
							<!-- END RIGHT COLUMN -->
						</div>
					</div>
				</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
	<?php include 'footer.php'?>