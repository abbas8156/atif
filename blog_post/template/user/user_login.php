<?php
include'../controller/user_controller.php';
include '../db_conn.php';
$user = new User_Controller($conn);
session_start();
if (isset($_SESSION['user'])) { header('location:index.php'); }
if($_POST){
	$post = $_POST;
	if(isset($post['email']) && !empty($post['email'])){
		$email = $post['email'];
	}
	else { $email_error = "Please Enter Email Address";
	}
if(isset($post['password']) && !empty($post['password'])){
		$password = $post['password'];
	}
	else{

		$passowrd_error = "Please Enter Password";
	}
	if(isset($email) && isset($password)){
		if ($user->loginUser($email,$password)) {
			header('location:index.php');
		}
		else {
			$login_error = '<div class = "alert alert-danger">Email or Password is incorrect.</div>';
		}
	}
}

?>

<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Login</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="../assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="../assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box ">
					<div class="left">
						<div class="content">
							<div class="header">
								<div class="logo text-center"><img src="assets/img/logo-dark.png" alt="Klorofil Logo"></div>
								<p class="lead">Login to your account</p>
								<?=isset($login_error)?$login_error:''?>
							</div>
							<form class="form-auth-small"  method="POST" action="user_login.php">
								<div class="form-group <?=isset($email_error)?'red':''?>">
									<label for="signin-email" class="control-label sr-only">Email</label>
									<input type="email" class="form-control  <?=isset($email_error)?'border_red':''?>" id="signin-email" value="<?=isset($email)?$email:''?>" placeholder="Email" name="email">
									<?=isset($email_error)?$email_error:''?>
								</div>
								<div class="form-group  <?=isset($passowrd_error)?'red':''?>">
									<label for="signin-password" class="control-label sr-only">Password</label>
									<input type="password" class="form-control  <?=isset($passowrd_error)?'border_red':''?>" id="signin-password" value="<?=isset($password)?$password:''?>" placeholder="Password" name="password">
									<?=isset($passowrd_error)?$passowrd_error:''?>
								</div>
								<div class="form-group clearfix">
									<label class="fancy-checkbox element-left">
										<input type="checkbox">
										<span>Remember me</span>
									</label>
								</div>
								<button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
								<div class="bottom">
									<span class="helper-text"><i class="fa fa-lock"></i> <a href="#">Forgot password?</a></span>
								</div>
							</form>
						</div>
					</div>
					<div class="right">
						<div class="overlay"></div>
						<div class="content text">
							<h1 class="heading">Free Bootstrap dashboard template</h1>
							<p>by The Develovers</p>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
