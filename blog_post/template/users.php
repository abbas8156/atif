<?php
include 'db_conn.php';
	include 'header.php';
	include 'controller/user_controller.php';

	$user = new User_controller($conn);
	if(isset($_GET['id'])){
  $id = $_GET['id'];
	$user->deleteUser($id);
}
if(isset($_GET['user_id']) && isset($_GET['status'])){
  $user_id = $_GET['user_id'];
  $status = $_GET['status'];
  $user->statusUser($user_id, $status);
  }
  $user_record = $user->listUser();
?>
		<body>
			<!-- WRAPPER -->
			<div id="wrapper">
		<!-- NAVBAR -->
				<?php include 'top_navigation.php'?>
					<!-- SIDE NAVBAR -->
					<?php include 'sidebar_navigation.php'?>
						<div class="main">
						<!-- 	<div class="col-md-6"> -->
							<!-- TABLE HOVER -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Hover Row</h3>
								</div>
								<div class="panel-body">
									<table class="table table-hover">
										<thead>
											<tr>
												<th>ID</th>
												<th>Name</th>
												<th>Email</th>
												<th>Phone Number</th>
												<th>Created At</th>
												<th>Updated At</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php while ($row = mysqli_fetch_array($user_record, MYSQLI_ASSOC)) {?>
       								 <tr>
       								 	<td><?=$row['user_id']?> </td>
        								<td><?=$row['user_name']?> </td>
							         	<td><?=$row['user_email']?></td>
							          	<td><?=$row['user_ph_num']?></td>
							          	<td><?=$row['created_at']?></td>
							          	<td><?=$row['updated_at']?></td>
							          	<td><a href="?user_id=<?=$row['user_id']?>&status=<?=$row['is_active']?0:1?>"><?=$row['is_active']?'Block':'Active'?></td>
							          	<td><a href="?id=<?=$row['user_id']?>">Delete</a></td>
							          	
							        </tr>
						  		<?php }?>
											
										</tbody>
									</table>
								</div>
							</div>
							<!-- END TABLE HOVER -->
						</div>
					</div>
						    	
							</table>
					<!-- 	</div> -->
			</div>
	
<?php include 'footer.php'?>